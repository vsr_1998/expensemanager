import path from "path";
import fs from "fs";

import React from "react";
import express from "express";
import ReactDOMServer from "react-dom/server";
import { StaticRouter } from 'react-router-dom';

import { Provider } from "react-redux";
import { createStore } from "redux";
import { listReducer } from "../src/redux/reducers.js";
import App from "../src/App.js";

const PORT = process.env.PORT || 3006;
const app = express();
app.use(express.static('./build'));
app.get('/*', (req, res) => {
    console.log("URL read!! " + req.url);
    const context = {};
    const store = createStore(listReducer)
    const app = ReactDOMServer.renderToString(
        <Provider store={store}>
            <StaticRouter location={req.url} context={context}>
                <App />
            </StaticRouter>
        </Provider>
    );
    const preloadedState = store.getState()
    // Send the rendered page back to the client
    const indexFile = path.resolve('./build/index.html');
    fs.readFile(indexFile, 'utf8', (err, data) => {
        console.log("Changed !!!");
        if (err) {
            console.error('Something went wrong:', err);
            return res.status(500).send('Oops, better luck next time!');
        }
        if (context.status === 404) {
            res.status(404);
        }
        if(context.url){
            return res.redirect(301, context.url);
        }
        return res.send(
            data.replace('<div id="root"></div>', 
            `<div id="root">${app}</div>
            <script>
            // WARNING: See the following for security issues around embedding JSON in HTML:
            // https://redux.js.org/recipes/server-rendering/#security-considerations
            window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(
                /</g,
                '\\u003c'
            )}
            </script>
            `)
        );
    });
});
app.listen(PORT, () => {
    console.log(`😎 Server is listening on port ${PORT}`);
});