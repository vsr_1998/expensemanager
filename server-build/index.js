/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./server/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./server/index.js":
/*!*************************!*\
  !*** ./server/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! fs */ \"fs\");\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-dom/server */ \"react-dom/server\");\n/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_dom_server__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var _src_redux_reducers_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../src/redux/reducers.js */ \"./src/redux/reducers.js\");\n/* harmony import */ var _src_App_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../src/App.js */ \"./src/App.js\");\n\n\n\n\n\n\n\n\n\n\nvar PORT = process.env.PORT || 3006;\nvar app = express__WEBPACK_IMPORTED_MODULE_3___default()();\napp.use(express__WEBPACK_IMPORTED_MODULE_3___default.a.static('./build'));\napp.get('/*', function (req, res) {\n  console.log(\"URL read!! \" + req.url);\n  var context = {};\n  var store = Object(redux__WEBPACK_IMPORTED_MODULE_7__[\"createStore\"])(_src_redux_reducers_js__WEBPACK_IMPORTED_MODULE_8__[\"listReducer\"]);\n  var app = react_dom_server__WEBPACK_IMPORTED_MODULE_4___default.a.renderToString(react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_6__[\"Provider\"], {\n    store: store\n  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__[\"StaticRouter\"], {\n    location: req.url,\n    context: context\n  }, react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_src_App_js__WEBPACK_IMPORTED_MODULE_9__[\"default\"], null))));\n  var preloadedState = store.getState(); // Send the rendered page back to the client\n\n  var indexFile = path__WEBPACK_IMPORTED_MODULE_0___default.a.resolve('./build/index.html');\n  fs__WEBPACK_IMPORTED_MODULE_1___default.a.readFile(indexFile, 'utf8', function (err, data) {\n    console.log(\"Changed !!!\");\n\n    if (err) {\n      console.error('Something went wrong:', err);\n      return res.status(500).send('Oops, better luck next time!');\n    }\n\n    if (context.status === 404) {\n      res.status(404);\n    }\n\n    if (context.url) {\n      return res.redirect(301, context.url);\n    }\n\n    return res.send(data.replace('<div id=\"root\"></div>', \"<div id=\\\"root\\\">\".concat(app, \"</div>\\n            <script>\\n            // WARNING: See the following for security issues around embedding JSON in HTML:\\n            // https://redux.js.org/recipes/server-rendering/#security-considerations\\n            window.__PRELOADED_STATE__ = \").concat(JSON.stringify(preloadedState).replace(/</g, \"\\\\u003c\"), \"\\n            </script>\\n            \")));\n  });\n});\napp.listen(PORT, function () {\n  console.log(\"\\uD83D\\uDE0E Server is listening on port \".concat(PORT));\n});\n\n//# sourceURL=webpack:///./server/index.js?");

/***/ }),

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return App; });\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _presentational_SignUp_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./presentational/SignUp.js */ \"./src/presentational/SignUp.js\");\n/* harmony import */ var _container_ShowLogin_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./container/ShowLogin.js */ \"./src/container/ShowLogin.js\");\n/* harmony import */ var _Main_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Main.js */ \"./src/Main.js\");\n/* harmony import */ var _NotFound_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./NotFound.js */ \"./src/NotFound.js\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var history__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! history */ \"history\");\n/* harmony import */ var history__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(history__WEBPACK_IMPORTED_MODULE_6__);\n\n\n\n\n\n\n\nvar history = Object(history__WEBPACK_IMPORTED_MODULE_6__[\"createMemoryHistory\"])();\nfunction App() {\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"App\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__[\"Switch\"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__[\"Route\"], {\n    exact: true,\n    path: \"/\",\n    component: _Main_js__WEBPACK_IMPORTED_MODULE_3__[\"default\"]\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__[\"Route\"], {\n    exact: true,\n    path: \"/userSignUp\",\n    component: _presentational_SignUp_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"]\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__[\"Route\"], {\n    exact: true,\n    path: \"/userLogIn\",\n    component: _container_ShowLogin_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"]\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__[\"Route\"], {\n    component: _NotFound_js__WEBPACK_IMPORTED_MODULE_4__[\"default\"]\n  })));\n}\n\n//# sourceURL=webpack:///./src/App.js?");

/***/ }),

/***/ "./src/Main.js":
/*!*********************!*\
  !*** ./src/Main.js ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Main; });\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _presentational_DescriptionBar_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./presentational/DescriptionBar.js */ \"./src/presentational/DescriptionBar.js\");\n/* harmony import */ var _container_ShowTables_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./container/ShowTables.js */ \"./src/container/ShowTables.js\");\n/* harmony import */ var _container_ShowFields__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./container/ShowFields */ \"./src/container/ShowFields.js\");\n/* harmony import */ var _presentational_UserAuth_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./presentational/UserAuth.js */ \"./src/presentational/UserAuth.js\");\n\n\n\n\n\nfunction Main() {\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_presentational_UserAuth_js__WEBPACK_IMPORTED_MODULE_4__[\"default\"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_container_ShowFields__WEBPACK_IMPORTED_MODULE_3__[\"default\"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_presentational_DescriptionBar_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_container_ShowTables_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"], null));\n}\n\n//# sourceURL=webpack:///./src/Main.js?");

/***/ }),

/***/ "./src/NotFound.js":
/*!*************************!*\
  !*** ./src/NotFound.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (function (_ref) {\n  var _ref$staticContext = _ref.staticContext,\n      staticContext = _ref$staticContext === void 0 ? {} : _ref$staticContext;\n  staticContext.status = 404;\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"h1\", null, \"Oops, nothing here!\"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n    type: \"submit\"\n  }, \"Home\")));\n});\n\n//# sourceURL=webpack:///./src/NotFound.js?");

/***/ }),

/***/ "./src/container/ShowFields.js":
/*!*************************************!*\
  !*** ./src/container/ShowFields.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _presentational_Fields__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../presentational/Fields */ \"./src/presentational/Fields.js\");\n\n\n\nvar mapStateToProps = function mapStateToProps(state) {\n  return {\n    incomeList: state.incomeList,\n    expenseList: state.expenseList\n  };\n};\n\nvar ShowFields = Object(react_redux__WEBPACK_IMPORTED_MODULE_0__[\"connect\"])(mapStateToProps, null)(_presentational_Fields__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n/* harmony default export */ __webpack_exports__[\"default\"] = (ShowFields);\n\n//# sourceURL=webpack:///./src/container/ShowFields.js?");

/***/ }),

/***/ "./src/container/ShowLogin.js":
/*!************************************!*\
  !*** ./src/container/ShowLogin.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _redux_actions_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../redux/actions.js */ \"./src/redux/actions.js\");\n/* harmony import */ var _presentational_Login__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../presentational/Login */ \"./src/presentational/Login.js\");\n\n\n\n\nvar mapDispatchToProps = function mapDispatchToProps(dispatch) {\n  return {\n    setIncome: function setIncome(list) {\n      return dispatch(Object(_redux_actions_js__WEBPACK_IMPORTED_MODULE_1__[\"setIncomeToState\"])(list));\n    },\n    setExpense: function setExpense(list) {\n      return dispatch(Object(_redux_actions_js__WEBPACK_IMPORTED_MODULE_1__[\"setExpenseToState\"])(list));\n    }\n  };\n};\n\nvar mapStateToProps = function mapStateToProps(state) {\n  return {\n    incomeList: state.incomeList,\n    expenseList: state.expenseList\n  };\n};\n\nvar ShowLogin = Object(react_redux__WEBPACK_IMPORTED_MODULE_0__[\"connect\"])(mapStateToProps, mapDispatchToProps)(_presentational_Login__WEBPACK_IMPORTED_MODULE_2__[\"default\"]);\n/* harmony default export */ __webpack_exports__[\"default\"] = (ShowLogin);\n\n//# sourceURL=webpack:///./src/container/ShowLogin.js?");

/***/ }),

/***/ "./src/container/ShowTables.js":
/*!*************************************!*\
  !*** ./src/container/ShowTables.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _presentational_Tables_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../presentational/Tables.js */ \"./src/presentational/Tables.js\");\n/* harmony import */ var _redux_actions_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../redux/actions.js */ \"./src/redux/actions.js\");\n\n\n\n\nvar mapStateToProps = function mapStateToProps(state) {\n  return {\n    incomeList: state.incomeList,\n    expenseList: state.expenseList\n  };\n};\n\nvar mapDispatchToProps = function mapDispatchToProps(dispatch) {\n  return {\n    deleteIncome: function deleteIncome(id) {\n      return dispatch(Object(_redux_actions_js__WEBPACK_IMPORTED_MODULE_2__[\"deleteFromIncome\"])(id));\n    },\n    deleteExpense: function deleteExpense(id) {\n      return dispatch(Object(_redux_actions_js__WEBPACK_IMPORTED_MODULE_2__[\"deleteFromExpense\"])(id));\n    }\n  };\n};\n\nvar ShowTables = Object(react_redux__WEBPACK_IMPORTED_MODULE_0__[\"connect\"])(mapStateToProps, mapDispatchToProps)(_presentational_Tables_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n/* harmony default export */ __webpack_exports__[\"default\"] = (ShowTables);\n\n//# sourceURL=webpack:///./src/container/ShowTables.js?");

/***/ }),

/***/ "./src/presentational/DescriptionBar.js":
/*!**********************************************!*\
  !*** ./src/presentational/DescriptionBar.js ***!
  \**********************************************/
/*! exports provided: DescriptionBar, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DescriptionBar\", function() { return DescriptionBar; });\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _redux_actions_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../redux/actions.js */ \"./src/redux/actions.js\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ \"react-redux\");\n/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\nvar DescriptionBar = function DescriptionBar(props) {\n  var textInput;\n  var numInput;\n\n  var handleSubmit = function handleSubmit(event) {\n    event.preventDefault();\n    var text = textInput.value;\n    var value = numInput.value;\n    textInput.value = \"\";\n    numInput.value = \"\";\n\n    if (text === \"\" || value === \"\") {\n      alert(\"Enter both description and value!\");\n      return;\n    }\n\n    if (!(\"currentUser\" in window.localStorage)) {\n      alert(\"Log in to continue!\");\n      return;\n    }\n\n    if (Number(value) <= 0) {\n      alert(\"Enter a positive non-zero number for value!\");\n      return;\n    }\n\n    var isPlusSignSelected = document.getElementById(\"plusID\").selected;\n    if (isPlusSignSelected) props.dispatch(Object(_redux_actions_js__WEBPACK_IMPORTED_MODULE_1__[\"addToIncome\"])(text, Number(value)));else props.dispatch(Object(_redux_actions_js__WEBPACK_IMPORTED_MODULE_1__[\"addToExpense\"])(text, -Number(value)));\n  };\n\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"form\", {\n    onSubmit: function onSubmit(e) {\n      return handleSubmit(e);\n    }\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"select\", {\n    id: \"selectorID\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"option\", {\n    id: \"plusID\",\n    defaultChecked: true\n  }, \"+\"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"option\", {\n    id: \"minusID\"\n  }, \"-\")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"input\", {\n    type: \"text\",\n    id: \"descriptionID\",\n    ref: function ref(node) {\n      return textInput = node;\n    },\n    placeholder: \"Description\"\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"input\", {\n    type: \"number\",\n    id: \"valueID\",\n    ref: function ref(node) {\n      return numInput = node;\n    },\n    placeholder: \"Value\"\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n    type: \"submit\"\n  }, \"Add\")));\n};\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__[\"connect\"])()(DescriptionBar));\n\n//# sourceURL=webpack:///./src/presentational/DescriptionBar.js?");

/***/ }),

/***/ "./src/presentational/Fields.js":
/*!**************************************!*\
  !*** ./src/presentational/Fields.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n\nvar date = new Date();\n\nvar getMonthByIndex = function getMonthByIndex(index) {\n  var months = [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\", \"July\", \"August\", \"September\", \"October\", \"November\", \"December\"];\n  return months[index];\n};\n\nvar ShowField = function ShowField(props) {\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    id: props.id,\n    className: \"FieldClass\"\n  }, props.name, \" \", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"object\", {\n    align: \"right\",\n    \"aria-label\": props.label\n  }, props.value));\n}; // Displays the remaining budget, income and expense\n// Budget receives props from Main component\n\n\nvar getSum = function getSum(items) {\n  if (items === undefined || items.length === 0) return 0;\n  return items.reduce(function (sum, item) {\n    return sum + item.value;\n  }, 0);\n};\n\nfunction Fields(props) {\n  var totalIncome = getSum(props.incomeList);\n  var totalExpense = getSum(props.expenseList);\n  var currentMoney = totalIncome + totalExpense;\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"topSection\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"h2\", null, \"Budget in \", getMonthByIndex(date.getMonth()), \" \", date.getFullYear()), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"h1\", {\n    \"aria-label\": \"totalMoney\",\n    id: \"totalBudgetID\"\n  }, currentMoney.toFixed(2)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ShowField, {\n    name: \"INCOME\",\n    value: totalIncome.toFixed(2),\n    id: \"incomeID\",\n    label: \"totalIncome\"\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"br\", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"br\", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ShowField, {\n    name: \"EXPENSE\",\n    value: totalExpense.toFixed(2),\n    id: \"expenseID\",\n    label: \"totalExpense\"\n  }));\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Fields);\n\n//# sourceURL=webpack:///./src/presentational/Fields.js?");

/***/ }),

/***/ "./src/presentational/Login.js":
/*!*************************************!*\
  !*** ./src/presentational/Login.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nfunction Login(props) {\n  var userName = \"\";\n  var userPass = \"\";\n  var rememberUser = false; // Handles the user input in the user name, password\n  // and remember me checkbox\n\n  var handleChange = function handleChange(event) {\n    switch (event.target.name) {\n      case \"uname\":\n        userName = event.target.value;\n        break;\n\n      case \"psw\":\n        userPass = event.target.value;\n        break;\n\n      case \"remember\":\n        rememberUser = event.target.checked;\n        break;\n\n      default:\n        return;\n    }\n  }; // When the user clicks Log In button, this function\n  // is called.\n\n\n  var handleSubmit = function handleSubmit(event) {\n    var usersObject = {};\n\n    if (\"users\" in window.localStorage) {\n      usersObject = JSON.parse(window.localStorage.getItem(\"users\"));\n    }\n\n    if (!usersObject.hasOwnProperty(userName)) {\n      alert(\"user name does not exist!\\nPlease sign-up\");\n      event.preventDefault();\n      return;\n    }\n\n    if (usersObject[userName] === userPass) {\n      alert(\"Welcome \".concat(userName, \"!\"));\n      window.localStorage.setItem(\"currentUser\", userName);\n      if (rememberUser) window.localStorage.setItem(\"rememberUser\", userName);\n\n      if (\"user+\" + userName in window.localStorage) {\n        var usersHistory = JSON.parse(window.localStorage.getItem(\"user+\" + userName));\n        props.setIncome(usersHistory.incomeList);\n        props.setExpense(usersHistory.expenseList);\n      }\n    } else alert(\"Incorrect password!\");\n  };\n\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"form\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"signUpContainer\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"label\", {\n    htmlFor: \"uname\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"b\", null, \"Username\")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"input\", {\n    type: \"text\",\n    placeholder: \"Enter Username\",\n    name: \"uname\",\n    \"aria-label\": \"uname_req\",\n    onChange: handleChange,\n    required: true\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"label\", {\n    htmlFor: \"psw\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"b\", null, \"Password\")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"input\", {\n    type: \"password\",\n    placeholder: \"Enter Password\",\n    name: \"psw\",\n    \"aria-label\": \"psw_req\",\n    onChange: handleChange,\n    required: true\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"label\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"input\", {\n    type: \"checkbox\",\n    name: \"remember\",\n    onChange: handleChange\n  }), \" \", \"Remember me\", \" \"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n    type: \"submit\",\n    \"data-testid\": \"login_btn\",\n    onClick: handleSubmit\n  }, \"Log In\")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n    type: \"submit\",\n    \"data-testid\": \"cancel_btn\"\n  }, \"Cancel\"))));\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Login);\n\n//# sourceURL=webpack:///./src/presentational/Login.js?");

/***/ }),

/***/ "./src/presentational/SignUp.js":
/*!**************************************!*\
  !*** ./src/presentational/SignUp.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nfunction SignUp() {\n  var userName = \"\";\n  var userPass = \"\"; // Handles the user input for user name and password\n\n  var handleChange = function handleChange(event) {\n    switch (event.target.name) {\n      case \"uname\":\n        userName = event.target.value;\n        break;\n\n      case \"psw\":\n        userPass = event.target.value;\n        break;\n\n      default:\n        return;\n    }\n  }; // Signs up user and adds the user to local storage\n\n\n  var handleSubmit = function handleSubmit(event) {\n    var usersObject = {};\n\n    if (userName === \"\" || userPass === \"\") {\n      alert(\"Can not leave either user name or password empty\");\n      event.preventDefault();\n      return;\n    }\n\n    if (userName.match(/[+]/)) {\n      alert(\"Can not create user name!\\nReason: + is a reserved symbol\");\n      event.preventDefault();\n      return;\n    }\n\n    if (\"users\" in localStorage) {\n      usersObject = JSON.parse(localStorage.getItem(\"users\"));\n    }\n\n    if (usersObject.hasOwnProperty(userName)) {\n      alert(\"user name exists!\");\n      event.preventDefault();\n      return;\n    }\n\n    alert(\"Thank You for signing up!\");\n    usersObject[userName] = userPass;\n    window.localStorage.setItem(\"users\", JSON.stringify(usersObject));\n  };\n\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"form\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    className: \"signUpContainer\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"label\", {\n    htmlFor: \"uname\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"b\", null, \"Username\")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"input\", {\n    type: \"text\",\n    placeholder: \"Enter Username\",\n    name: \"uname\",\n    onChange: handleChange,\n    \"aria-label\": \"uname_req\",\n    required: true\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"label\", {\n    htmlFor: \"psw\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"b\", null, \"Password\")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"input\", {\n    type: \"password\",\n    placeholder: \"Enter Password\",\n    name: \"psw\",\n    onChange: handleChange,\n    \"aria-label\": \"psw_req\",\n    required: true\n  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n    type: \"submit\",\n    \"data-testid\": \"signup_btn\",\n    onClick: handleSubmit\n  }, \"Sign Up\")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n    type: \"submit\",\n    \"data-testid\": \"cancel_btn\"\n  }, \"Cancel\"))));\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (SignUp);\n\n//# sourceURL=webpack:///./src/presentational/SignUp.js?");

/***/ }),

/***/ "./src/presentational/Tables.js":
/*!**************************************!*\
  !*** ./src/presentational/Tables.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n\n\nfunction Tables(props) {\n  function showRow(description, value, index) {\n    var deleteRow = function deleteRow(value, index) {\n      if (value < 0) props.deleteExpense(index);else props.deleteIncome(index);\n    };\n\n    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"tr\", {\n      key: index,\n      \"data-testid\": index\n    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"td\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"object\", {\n      style: {\n        float: \"left\",\n        fontSize: 24\n      }\n    }, description), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"object\", {\n      style: {\n        float: \"right\",\n        fontSize: 24\n      }\n    }, value)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n      type: \"button\",\n      onClick: function onClick() {\n        return deleteRow(value, index);\n      }\n    }, \"X\"));\n  }\n\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    style: {\n      textAlign: \"center\",\n      margin: \"10px 0px 0px 0px\"\n    }\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"table\", {\n    \"data-testid\": \"incomeTable\",\n    id: \"incomeListID\",\n    style: {\n      margin: \"0px 30px 0px 0px\"\n    }\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"thead\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"tr\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"th\", {\n    style: {\n      color: \"purple\",\n      fontSize: 24\n    }\n  }, \"Income\"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"tbody\", null, props.incomeList.map(function (params) {\n    return showRow(params.text, params.value, params.index);\n  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"table\", {\n    \"data-testid\": \"expenseTable\",\n    id: \"expenseListID\",\n    style: {\n      margin: \"0px 0px 0px 30px\"\n    }\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"thead\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"tr\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"th\", {\n    style: {\n      color: \"red\",\n      fontSize: 24\n    }\n  }, \"Expense\"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"tbody\", null, props.expenseList.map(function (params) {\n    return showRow(params.text, params.value, params.index);\n  }))));\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Tables);\n\n//# sourceURL=webpack:///./src/presentational/Tables.js?");

/***/ }),

/***/ "./src/presentational/UserAuth.js":
/*!****************************************!*\
  !*** ./src/presentational/UserAuth.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);\nfunction _typeof(obj) { \"@babel/helpers - typeof\"; if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\n\n\n\nvar RenderButtons = function RenderButtons() {\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", {\n    style: {\n      display: \"inline\",\n      float: \"left\"\n    }\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/userSignUp\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n    type: \"button\"\n  }, \"Sign Up\")), \" \", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/userLogIn\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n    type: \"button\"\n  }, \"Log In\")));\n};\n\nvar deleteUser = function deleteUser(event) {\n  if (\"rememberUser\" in window.localStorage) window.localStorage.removeItem(\"rememberUser\");\n  window.location.reload(true);\n};\n\nvar RenderUser = function RenderUser(props) {\n  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"div\", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"p\", {\n    style: {\n      fontSize: 20,\n      color: \"red\"\n    }\n  }, \"Hello, \", props.userName), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__[\"Link\"], {\n    to: \"/\"\n  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(\"button\", {\n    type: \"submit\",\n    onClick: deleteUser\n  }, \"Log-Out\")));\n};\n\nvar UserAuth =\n/*#__PURE__*/\nfunction (_Component) {\n  _inherits(UserAuth, _Component);\n\n  function UserAuth() {\n    var _this;\n\n    _classCallCheck(this, UserAuth);\n\n    _this = _possibleConstructorReturn(this, _getPrototypeOf(UserAuth).call(this));\n    _this.state = {\n      whatToRender: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(RenderButtons, null)\n    };\n    return _this;\n  }\n\n  _createClass(UserAuth, [{\n    key: \"componentDidMount\",\n    value: function componentDidMount() {\n      var currentUser = window.localStorage.getItem(\"currentUser\");\n      var rememberUser = window.localStorage.getItem(\"rememberUser\");\n\n      if (currentUser) {\n        this.setState({\n          whatToRender: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(RenderUser, {\n            userName: currentUser\n          })\n        });\n      } else if (rememberUser) {\n        this.setState({\n          whatToRender: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(RenderUser, {\n            userName: rememberUser\n          })\n        });\n        window.localStorage.setItem(\"currentUser\", rememberUser);\n      }\n    }\n  }, {\n    key: \"render\",\n    value: function render() {\n      return this.state.whatToRender;\n    }\n  }]);\n\n  return UserAuth;\n}(react__WEBPACK_IMPORTED_MODULE_0__[\"Component\"]);\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (UserAuth);\n\n//# sourceURL=webpack:///./src/presentational/UserAuth.js?");

/***/ }),

/***/ "./src/redux/actions.js":
/*!******************************!*\
  !*** ./src/redux/actions.js ***!
  \******************************/
/*! exports provided: ADD_TO_INCOME_LIST, ADD_TO_EXPENSE_LIST, DELETE_FROM_INCOME, DELETE_FROM_EXPENSE, SET_INCOME_TO_STATE, SET_EXPENSE_TO_STATE, addToIncome, addToExpense, deleteFromIncome, deleteFromExpense, setIncomeToState, setExpenseToState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ADD_TO_INCOME_LIST\", function() { return ADD_TO_INCOME_LIST; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ADD_TO_EXPENSE_LIST\", function() { return ADD_TO_EXPENSE_LIST; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DELETE_FROM_INCOME\", function() { return DELETE_FROM_INCOME; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DELETE_FROM_EXPENSE\", function() { return DELETE_FROM_EXPENSE; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SET_INCOME_TO_STATE\", function() { return SET_INCOME_TO_STATE; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SET_EXPENSE_TO_STATE\", function() { return SET_EXPENSE_TO_STATE; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addToIncome\", function() { return addToIncome; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addToExpense\", function() { return addToExpense; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"deleteFromIncome\", function() { return deleteFromIncome; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"deleteFromExpense\", function() { return deleteFromExpense; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"setIncomeToState\", function() { return setIncomeToState; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"setExpenseToState\", function() { return setExpenseToState; });\n/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uuid */ \"uuid\");\n/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(uuid__WEBPACK_IMPORTED_MODULE_0__);\n\nvar ADD_TO_INCOME_LIST = \"ADD_TO_INCOME_LIST\";\nvar ADD_TO_EXPENSE_LIST = \"ADD_TO_EXPENSE_LIST\";\nvar DELETE_FROM_INCOME = \"DELETE_FROM_INCOME\";\nvar DELETE_FROM_EXPENSE = \"DELETE_FROM_EXPENSE\";\nvar SET_INCOME_TO_STATE = \"SET_INCOME_TO_STATE\";\nvar SET_EXPENSE_TO_STATE = \"SET_EXPENSE_TO_STATE\";\nvar addToIncome = function addToIncome(text, value) {\n  return {\n    type: ADD_TO_INCOME_LIST,\n    text: text,\n    value: value,\n    index: uuid__WEBPACK_IMPORTED_MODULE_0___default()()\n  };\n};\nvar addToExpense = function addToExpense(text, value) {\n  return {\n    type: ADD_TO_EXPENSE_LIST,\n    text: text,\n    value: value,\n    index: uuid__WEBPACK_IMPORTED_MODULE_0___default()()\n  };\n};\nvar deleteFromIncome = function deleteFromIncome(index) {\n  return {\n    type: DELETE_FROM_INCOME,\n    index: index\n  };\n};\nvar deleteFromExpense = function deleteFromExpense(index) {\n  return {\n    type: DELETE_FROM_EXPENSE,\n    index: index\n  };\n};\nvar setIncomeToState = function setIncomeToState(list) {\n  return {\n    type: SET_INCOME_TO_STATE,\n    list: list\n  };\n};\nvar setExpenseToState = function setExpenseToState(list) {\n  return {\n    type: SET_EXPENSE_TO_STATE,\n    list: list\n  };\n};\n\n//# sourceURL=webpack:///./src/redux/actions.js?");

/***/ }),

/***/ "./src/redux/reducers.js":
/*!*******************************!*\
  !*** ./src/redux/reducers.js ***!
  \*******************************/
/*! exports provided: listReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"listReducer\", function() { return listReducer; });\n/* harmony import */ var _actions_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./actions.js */ \"./src/redux/actions.js\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux */ \"redux\");\n/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_1__);\nfunction _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }\n\nfunction _nonIterableSpread() { throw new TypeError(\"Invalid attempt to spread non-iterable instance\"); }\n\nfunction _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === \"[object Arguments]\") return Array.from(iter); }\n\nfunction _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }\n\n\n\n\nvar incomeList = function incomeList() {\n  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];\n  var action = arguments.length > 1 ? arguments[1] : undefined;\n\n  switch (action.type) {\n    case _actions_js__WEBPACK_IMPORTED_MODULE_0__[\"ADD_TO_INCOME_LIST\"]:\n      return [].concat(_toConsumableArray(state), [{\n        text: action.text,\n        value: action.value,\n        index: action.index\n      }]);\n\n    case _actions_js__WEBPACK_IMPORTED_MODULE_0__[\"DELETE_FROM_INCOME\"]:\n      return state.filter(function (item) {\n        return item.index !== action.index;\n      });\n\n    case _actions_js__WEBPACK_IMPORTED_MODULE_0__[\"SET_INCOME_TO_STATE\"]:\n      return state.concat(action.list);\n\n    default:\n      return state;\n  }\n};\n\nvar expenseList = function expenseList() {\n  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];\n  var action = arguments.length > 1 ? arguments[1] : undefined;\n\n  switch (action.type) {\n    case _actions_js__WEBPACK_IMPORTED_MODULE_0__[\"ADD_TO_EXPENSE_LIST\"]:\n      return [].concat(_toConsumableArray(state), [{\n        text: action.text,\n        value: action.value,\n        index: action.index\n      }]);\n\n    case _actions_js__WEBPACK_IMPORTED_MODULE_0__[\"DELETE_FROM_EXPENSE\"]:\n      return state.filter(function (item) {\n        return item.index !== action.index;\n      });\n\n    case _actions_js__WEBPACK_IMPORTED_MODULE_0__[\"SET_EXPENSE_TO_STATE\"]:\n      return state.concat(action.list);\n\n    default:\n      return state;\n  }\n};\n\nvar listReducer = Object(redux__WEBPACK_IMPORTED_MODULE_1__[\"combineReducers\"])({\n  incomeList: incomeList,\n  expenseList: expenseList\n});\n\n//# sourceURL=webpack:///./src/redux/reducers.js?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"fs\");\n\n//# sourceURL=webpack:///external_%22fs%22?");

/***/ }),

/***/ "history":
/*!**************************!*\
  !*** external "history" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"history\");\n\n//# sourceURL=webpack:///external_%22history%22?");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n\n//# sourceURL=webpack:///external_%22path%22?");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");\n\n//# sourceURL=webpack:///external_%22react%22?");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-dom/server\");\n\n//# sourceURL=webpack:///external_%22react-dom/server%22?");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-redux\");\n\n//# sourceURL=webpack:///external_%22react-redux%22?");

/***/ }),

/***/ "react-router-dom":
/*!***********************************!*\
  !*** external "react-router-dom" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-router-dom\");\n\n//# sourceURL=webpack:///external_%22react-router-dom%22?");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"redux\");\n\n//# sourceURL=webpack:///external_%22redux%22?");

/***/ }),

/***/ "uuid":
/*!***********************!*\
  !*** external "uuid" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"uuid\");\n\n//# sourceURL=webpack:///external_%22uuid%22?");

/***/ })

/******/ });