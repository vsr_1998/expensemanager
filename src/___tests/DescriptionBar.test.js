import React from "react";
import {DescriptionBar} from "../presentational/DescriptionBar.js";
import {render, fireEvent, getByText} from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";


test("Chooses the correct option", () => {
    const utils = render(<DescriptionBar />);
    expect(utils.getByRole("listbox")).toHaveTextContent("+");
    fireEvent.click(utils.getByText("-"));
    expect(utils.getByRole("listbox")).toHaveTextContent("-");
});

test("Empty input fields", () => {
    const utils = render(<DescriptionBar />);
    expect(utils.getByPlaceholderText("Description")).toHaveTextContent("");
    expect(utils.getByPlaceholderText("Value")).toHaveTextContent("");
});
