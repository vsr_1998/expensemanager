import React from "react";
import SignUp from "../presentational/SignUp";
import {render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter, Router} from "react-router-dom";
import { createMemoryHistory } from 'history';
import { LocalStorageMock } from "@react-mock/localstorage";

test("Cancel button sends user to home page", () => {
    const history = createMemoryHistory();
    history.push("/userSignUp");
    const utils = render(<Router history={history}><SignUp /></Router>);
    expect(history.location.pathname).toBe("/userSignUp");
    const cancel = utils.getByTestId("cancel_btn");
    fireEvent.click(cancel);
    expect(history.location.pathname).toBe("/");
});

test("Empty fields", () => {
    
    const history = createMemoryHistory();
    history.push("/userSignUp");
    const utils = render(<Router history={history}><SignUp /></Router>);
    expect(utils.getByLabelText("uname_req").value).toBe("");
    expect(utils.getByLabelText("psw_req").value).toBe("");

});

test("Doesn't allow a user to sign up with a plus sign in username", () => {
    const jsdomAlert = window.alert;
    window.alert = () => {};
    const history = createMemoryHistory();
    history.push("/userSignUp");
    const utils = render(<Router history={history}><SignUp /></Router>);
    expect(utils.getByLabelText("uname_req").value).toBe("");
    expect(utils.getByLabelText("psw_req").value).toBe("");
    
    fireEvent.change(utils.getByLabelText("uname_req"), {
        target: {value: 'a+'},
    })
    fireEvent.change(utils.getByLabelText("psw_req"), {
        target: {value: '1'},
    })
    const button = utils.getByTestId("signup_btn");
    fireEvent.click(button);
    expect(history.location.pathname).toBe("/userSignUp");

    window.alert = jsdomAlert;
});

test("Doesn't allow a user to sign up without entering either username or password", () => {
    const jsdomAlert = window.alert;
    window.alert = () => {};
    const history = createMemoryHistory();
    history.push("/userSignUp");
    const utils = render(<Router history={history}><SignUp /></Router>);
    expect(utils.getByLabelText("uname_req").value).toBe("");
    expect(utils.getByLabelText("psw_req").value).toBe("");

    const button = utils.getByTestId("signup_btn");
    fireEvent.click(button);
    expect(history.location.pathname).toBe("/userSignUp");
    window.alert = jsdomAlert;
});

test("Adds user data to local storage", () => {
    const jsdomAlert = window.alert;
    window.alert = () => {};
    const history = createMemoryHistory();
    history.push("/userSignUp");
    const utils = render(
    <LocalStorageMock items ={{}} >
        <Router history={history}><SignUp /></Router>
    </LocalStorageMock>);
    expect(utils.getByLabelText("uname_req").value).toBe("");
    expect(utils.getByLabelText("psw_req").value).toBe("");

    fireEvent.change(utils.getByLabelText("uname_req"), {
        target: {value: 'r'},
    })
    fireEvent.change(utils.getByLabelText("psw_req"), {
        target: {value: '1'},
    })

    const button = utils.getByTestId("signup_btn");
    fireEvent.click(button);
    expect(JSON.parse(localStorage.getItem("users"))).toStrictEqual({"r" : "1"});
    window.alert = jsdomAlert;
});