import React from "react";
import Tables from "../presentational/Tables.js";
import {render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

test("Tables show correct number of rows", () => {
    const utils = render(<Tables incomeList={[{text:"a", value: 10, index: "0"}]} 
                                 expenseList={[{text:"b", value: -10, index: "0"}]}/>);
    let incomeTable = utils.getByTestId("incomeTable");
    let expenseTable = utils.getByTestId("expenseTable");
    expect(expenseTable.getElementsByTagName("tr").length).toStrictEqual(2);
});

test("Button deletes the selected row", () => {

    const deleteIncome = jest.fn();
    const deleteExpense = jest.fn();
    const utils = render(<Tables incomeList={[{text:"a", value: 10, index: "0"}]} 
                                 expenseList={[{text:"b", value: -10, index: "0"}]}
                                 deleteIncome={deleteIncome}
                                 deleteExpense={deleteExpense}/>);
    let incomeTable = utils.getByTestId("incomeTable");
    let expenseTable = utils.getByTestId("expenseTable");
    fireEvent.click(expenseTable.getElementsByTagName("button")[0]);
    expect(deleteExpense).toHaveBeenCalled();
    expect(deleteExpense).toHaveBeenCalledTimes(1);
});
