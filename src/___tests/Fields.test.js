import React from "react";
import Fields from "../presentational/Fields.js";
import {render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";


test("All fields show numbers", () => {
    const utils = render(<Fields />);
    const budget = utils.getByLabelText("totalMoney");
    const income = utils.getByLabelText("totalIncome");
    const expense = utils.getByLabelText("totalExpense");
    expect(isNaN(Number(budget.innerHTML))).toBe(false);
    expect(isNaN(Number(income.innerHTML))).toBe(false);
    expect(isNaN(Number(expense.innerHTML))).toBe(false);
});


test("All fields show correct values", () => {
    const incomeList = [{text: "A", value: 10, index:"0"},
                        {text: "A", value: 10, index:"1"},
                        {text: "A", value: 10, index:"2"} ];
                        
    const expenseList = [{text: "B", value: -2, index:"0"} ];
    const utils = render(<Fields incomeList={incomeList} expenseList={expenseList}/>);
    const budget = utils.getByLabelText("totalMoney");
    const income = utils.getByLabelText("totalIncome");
    const expense = utils.getByLabelText("totalExpense");
    expect(Number(budget.innerHTML)).toStrictEqual(28);
    expect(Number(income.innerHTML)).toStrictEqual(30);
    expect(Number(expense.innerHTML)).toStrictEqual(-2);
});
