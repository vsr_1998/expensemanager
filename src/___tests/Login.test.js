import React from "react";
import Login from "../presentational/Login.js";
import {render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter, Router} from "react-router-dom";
import { createMemoryHistory } from 'history';


test("Empty input fields", () => {
    const utils = render(<BrowserRouter><Login /></BrowserRouter>);
    expect(utils.getByLabelText("uname_req").value).toBe("");
    expect(utils.getByLabelText("psw_req").value).toBe("");
});

test("Connected to redux store", () => {
    const utils = render(<BrowserRouter><Login /></BrowserRouter>);
    expect(utils.container.isConnected).toBe(true);
});

test("Cancel button sends user to home page", () => {
    const history = createMemoryHistory();
    history.push("/userLogIn");
    const utils = render(<Router history={history}><Login /></Router>);
    expect(history.location.pathname).toBe("/userLogIn");
    const cancel = utils.getByTestId("cancel_btn");
    fireEvent.click(cancel);
    expect(history.location.pathname).toBe("/");
});

test("Should not allow log in if user has not entered correct credentials", () => {
    const jsdomAlert = window.alert;
    window.alert = () => {}; 
    const history = createMemoryHistory();
    history.push("/userLogIn");
    const utils = render(<Router history={history}><Login /></Router>);
    const login = utils.getByTestId("login_btn");
    fireEvent.click(login);
    expect(history.location.pathname).toBe("/userLogIn");
    window.alert = jsdomAlert;
});
