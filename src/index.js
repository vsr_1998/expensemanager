import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { listReducer } from "./redux/reducers.js";
import { BrowserRouter } from "react-router-dom";

let persistedState = window.__PRELOADED_STATE__;

if ("user+" + window.localStorage.getItem("currentUser") in window.localStorage)
  persistedState = JSON.parse(
    window.localStorage.getItem(
      "user+" + window.localStorage.getItem("currentUser")
    )
  );
else if (
  "user+" + window.localStorage.getItem("rememberUser") in
  window.localStorage
)
  persistedState = JSON.parse(
    window.localStorage.getItem(
      "user+" + window.localStorage.getItem("rememberUser")
    )
  );

const store = createStore(listReducer, persistedState);
window.addEventListener("beforeunload", () => {
  if ("currentUser" in window.localStorage) {
    let userHistory = "user+" + window.localStorage.getItem("currentUser");
    window.localStorage.setItem(userHistory, JSON.stringify(store.getState()));
  }
  window.localStorage.removeItem("currentUser");
});

const rootElement = document.getElementById("root");
ReactDOM.hydrate(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  rootElement
);
