import React from "react";
import SignUp from "./presentational/SignUp.js";
import ShowLogin from "./container/ShowLogin.js";
import Main from "./Main.js";
import NotFound from "./NotFound.js";
import { Switch, Route } from "react-router-dom";
import { createMemoryHistory } from 'history';

const history = createMemoryHistory();

export default function App() {
  return (
    <div className="App">
      <Switch>
          <Route exact path="/" component={Main} />
          <Route exact path="/userSignUp" component={SignUp} />
          <Route exact path="/userLogIn" component={ShowLogin} />
          <Route component={NotFound} />
      </Switch>
      <p>OK!</p>
    </div>
  );
}
