import React from "react";
import DescriptionBar from "./presentational/DescriptionBar.js";
import ShowTables from "./container/ShowTables.js";
import ShowFields from "./container/ShowFields";
import UserAuth from "./presentational/UserAuth.js";

export default function Main() {
  return (
    <>
      <UserAuth />
      <ShowFields />
      <DescriptionBar />
      <ShowTables />
    </>
  );
}
