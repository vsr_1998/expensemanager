import { connect } from "react-redux";
import Fields from "../presentational/Fields";

const mapStateToProps = state => {
  return {
    incomeList: state.incomeList,
    expenseList: state.expenseList
  };
};
const ShowFields = connect(
  mapStateToProps,
  null
)(Fields);
export default ShowFields;
