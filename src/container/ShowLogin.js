import { connect } from "react-redux";
import { setIncomeToState, setExpenseToState } from "../redux/actions.js";
import Login from "../presentational/Login";

const mapDispatchToProps = dispatch => {
  return {
    setIncome: list => dispatch(setIncomeToState(list)),
    setExpense: list => dispatch(setExpenseToState(list))
  };
};
const mapStateToProps = state => {
  return {
    incomeList: state.incomeList,
    expenseList: state.expenseList
  };
};
const ShowLogin = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
export default ShowLogin;
