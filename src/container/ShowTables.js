import { connect } from "react-redux";
import Tables from "../presentational/Tables.js";
import { deleteFromExpense, deleteFromIncome } from "../redux/actions.js";

const mapStateToProps = state => {
  return {
    incomeList: state.incomeList,
    expenseList: state.expenseList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteIncome: id => dispatch(deleteFromIncome(id)),
    deleteExpense: id => dispatch(deleteFromExpense(id))
  };
};
const ShowTables = connect(
  mapStateToProps,
  mapDispatchToProps
)(Tables);
export default ShowTables;
