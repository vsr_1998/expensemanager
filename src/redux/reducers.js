import {
  ADD_TO_EXPENSE_LIST,
  ADD_TO_INCOME_LIST,
  DELETE_FROM_INCOME,
  DELETE_FROM_EXPENSE,
  SET_EXPENSE_TO_STATE,
  SET_INCOME_TO_STATE
} from "./actions.js";
import { combineReducers } from "redux";

const incomeList = (state = [], action) => {
  switch (action.type) {
    case ADD_TO_INCOME_LIST:
      return [
        ...state,
        {
          text: action.text,
          value: action.value,
          index: action.index
        }
      ];
    case DELETE_FROM_INCOME:
      return state.filter(item => item.index !== action.index);
    case SET_INCOME_TO_STATE:
      return state.concat(action.list);
    default:
      return state;
  }
};
const expenseList = (state = [], action) => {
  switch (action.type) {
    case ADD_TO_EXPENSE_LIST:
      return [
        ...state,
        {
          text: action.text,
          value: action.value,
          index: action.index
        }
      ];
    case DELETE_FROM_EXPENSE:
      return state.filter(item => item.index !== action.index);
    case SET_EXPENSE_TO_STATE:
      return state.concat(action.list);
    default:
      return state;
  }
};
export const listReducer = combineReducers({
  incomeList,
  expenseList
});
