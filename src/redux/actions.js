import uuid from "uuid";

export const ADD_TO_INCOME_LIST = "ADD_TO_INCOME_LIST";
export const ADD_TO_EXPENSE_LIST = "ADD_TO_EXPENSE_LIST";
export const DELETE_FROM_INCOME = "DELETE_FROM_INCOME";
export const DELETE_FROM_EXPENSE = "DELETE_FROM_EXPENSE";
export const SET_INCOME_TO_STATE = "SET_INCOME_TO_STATE";
export const SET_EXPENSE_TO_STATE = "SET_EXPENSE_TO_STATE";

export const addToIncome = (text, value) => {
  return {
    type: ADD_TO_INCOME_LIST,
    text,
    value,
    index: uuid()
  };
};
export const addToExpense = (text, value) => {
  return {
    type: ADD_TO_EXPENSE_LIST,
    text,
    value,
    index: uuid()
  };
};

export const deleteFromIncome = index => {
  return {
    type: DELETE_FROM_INCOME,
    index
  };
};
export const deleteFromExpense = index => {
  return {
    type: DELETE_FROM_EXPENSE,
    index
  };
};

export const setIncomeToState = list => {
  return {
    type: SET_INCOME_TO_STATE,
    list
  };
};
export const setExpenseToState = list => {
  return {
    type: SET_EXPENSE_TO_STATE,
    list
  };
};
