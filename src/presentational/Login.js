import React from "react";
import { Link } from "react-router-dom";

function Login(props) {
  var userName = "";
  var userPass = "";
  var rememberUser = false;
  // Handles the user input in the user name, password
  // and remember me checkbox
  const handleChange = event => {
    switch (event.target.name) {
      case "uname":
        userName = event.target.value;
        break;
      case "psw":
        userPass = event.target.value;
        break;
      case "remember":
        rememberUser = event.target.checked;
        break;
      default:
        return;
    }
  };
  // When the user clicks Log In button, this function
  // is called.
  const handleSubmit = event => {
    let usersObject = {};
    if ("users" in window.localStorage) {
      usersObject = JSON.parse(window.localStorage.getItem("users"));
    }
    if (!usersObject.hasOwnProperty(userName)) {
      alert("user name does not exist!\nPlease sign-up");
      event.preventDefault();
      return;
    }

    if (usersObject[userName] === userPass) {
      alert(`Welcome ${userName}!`);
      window.localStorage.setItem("currentUser", userName);
      if (rememberUser) window.localStorage.setItem("rememberUser", userName);
      if ("user+" + userName in window.localStorage) {
        let usersHistory = JSON.parse(
          window.localStorage.getItem("user+" + userName)
        );
        props.setIncome(usersHistory.incomeList);
        props.setExpense(usersHistory.expenseList);
      }
    } else alert("Incorrect password!");
  };
  return (
    <form>
      <div className="signUpContainer">
        <label htmlFor="uname">
          <b>Username</b>
        </label>
        <input
          type="text"
          placeholder="Enter Username"
          name="uname"
          aria-label="uname_req"
          onChange={handleChange}
          required
        />

        <label htmlFor="psw">
          <b>Password</b>
        </label>
        <input
          type="password"
          placeholder="Enter Password"
          name="psw"
          aria-label="psw_req"
          onChange={handleChange}
          required
        />
        <label>
          <input type="checkbox" name="remember" onChange={handleChange} />{" "}
          Remember me{" "}
        </label>

        <Link to="/">
          <button type="submit" data-testid="login_btn" onClick={handleSubmit}>
            Log In
          </button>
        </Link>
        <Link to="/">
          <button type="submit" data-testid="cancel_btn">Cancel</button>
        </Link>
      </div>
    </form>
  );
}

export default Login;
