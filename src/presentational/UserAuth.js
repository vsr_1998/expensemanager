import React, { Component } from "react";
import { Link } from "react-router-dom";

const RenderButtons = () => {
  return (
    <div
      style={{
        display: "inline",
        float: "left"
      }}
    >
      <Link to="/userSignUp">
        <button type="button">Sign Up</button>
      </Link>{" "}
      <Link to="/userLogIn">
        <button type="button">Log In</button>
      </Link>
    </div>
  );
};
const deleteUser = event => {
  if ("rememberUser" in window.localStorage)
    window.localStorage.removeItem("rememberUser");
  window.location.reload(true);
};
const RenderUser = props => {
  return (
    <div>
      <p
        style={{
          fontSize: 20,
          color: "red"
        }}
      >
        Hello, {props.userName}
      </p>
      <Link to="/">
        <button type="submit" onClick={deleteUser}>
          Log-Out
        </button>
      </Link>
    </div>
  );
};

class UserAuth extends Component {
  constructor(){
    super();
    this.state = {
      whatToRender: <RenderButtons />
    }
  }
  componentDidMount(){
    let currentUser = window.localStorage.getItem("currentUser");
    let rememberUser = window.localStorage.getItem("rememberUser");
    if(currentUser){
      this.setState({
        whatToRender: <RenderUser userName={currentUser} />
      });
    }
    else if (rememberUser){
      this.setState({
        whatToRender: <RenderUser userName={rememberUser} />
      });
      window.localStorage.setItem("currentUser", rememberUser);
    }
  }
  render() {
    return this.state.whatToRender;
  }
}

export default UserAuth;
