import React from "react";
import { Link } from "react-router-dom";
function SignUp() {
  var userName = "";
  var userPass = "";
  // Handles the user input for user name and password
  const handleChange = event => {
    switch (event.target.name) {
      case "uname":
        userName = event.target.value;
        break;
      case "psw":
        userPass = event.target.value;
        break;
      default:
        return;
    }
  };
  // Signs up user and adds the user to local storage
  const handleSubmit = event => {
    let usersObject = {};
    if(userName === "" || userPass === ""){
      alert("Can not leave either user name or password empty");
      event.preventDefault();
      return;
    }
    if (userName.match(/[+]/)) {
      alert("Can not create user name!\nReason: + is a reserved symbol");
      
      event.preventDefault();
      return;
    }
    if ("users" in localStorage) {
      usersObject = JSON.parse(localStorage.getItem("users"));
    }
    if (usersObject.hasOwnProperty(userName)) {
      alert("user name exists!");
      
      event.preventDefault();
      return;
    }
    alert("Thank You for signing up!");
    usersObject[userName] = userPass;
    window.localStorage.setItem("users", JSON.stringify(usersObject));
  };

  return (
    <form>
      <div className="signUpContainer">
        <label htmlFor="uname">
          <b>Username</b>
        </label>
        <input
          type="text"
          placeholder="Enter Username"
          name="uname"
          onChange={handleChange}
          aria-label="uname_req"
          required
        />

        <label htmlFor="psw">
          <b>Password</b>
        </label>
        <input
          type="password"
          placeholder="Enter Password"
          name="psw"
          onChange={handleChange}
          aria-label="psw_req"
          required
        />
        <Link to="/">
          <button type="submit" data-testid="signup_btn" onClick={handleSubmit}>
            Sign Up
          </button>
        </Link>
        <Link to="/">
          <button type="submit" data-testid="cancel_btn">Cancel</button>
        </Link>
      </div>
    </form>
  );
}

export default SignUp;
