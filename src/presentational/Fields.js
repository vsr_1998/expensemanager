import React from "react";
const date = new Date();

const getMonthByIndex = index => {
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  return months[index];
};
const ShowField = props => {
  return (
    <div id={props.id} className="FieldClass">
      {props.name} <object align="right" aria-label={props.label}>{props.value}</object>
    </div>
  );
};
// Displays the remaining budget, income and expense
// Budget receives props from Main component
const getSum = items => {
  if(items === undefined || items.length === 0)
    return 0;
  return items.reduce((sum, item) => sum + item.value, 0);
};
function Fields(props) {
  let totalIncome = getSum(props.incomeList);
  let totalExpense = getSum(props.expenseList);
  let currentMoney = totalIncome + totalExpense;

  return (
    <div className="topSection">
      <h2>
        Budget in {getMonthByIndex(date.getMonth())} {date.getFullYear()}
      </h2>
      <h1 aria-label="totalMoney" id="totalBudgetID">{currentMoney.toFixed(2)}</h1>
      <ShowField name="INCOME" value={totalIncome.toFixed(2)} id="incomeID" label="totalIncome"/>
      <br />
      <br />
      <ShowField
        name="EXPENSE"
        value={totalExpense.toFixed(2)}
        id="expenseID"
        label="totalExpense"
      />
    </div>
  );
}

export default Fields;
