import React from "react";
import { addToIncome, addToExpense } from "../redux/actions.js";
import { connect } from "react-redux";

export let DescriptionBar = props => {
  let textInput;
  let numInput;
  let handleSubmit = event => {
    event.preventDefault();
    let text = textInput.value;
    let value = numInput.value;
    textInput.value = "";
    numInput.value = "";
    if (text === "" || value === "") {
      alert("Enter both description and value!");
      return;
    }
    if (!("currentUser" in window.localStorage)) {
      alert("Log in to continue!");
      return;
    }
    if (Number(value) <= 0) {
      alert("Enter a positive non-zero number for value!");
      return;
    }
    let isPlusSignSelected = document.getElementById("plusID").selected;
    if (isPlusSignSelected) props.dispatch(addToIncome(text, Number(value)));
    else props.dispatch(addToExpense(text, -Number(value)));
  };

  return (
    <>
      <form onSubmit={e => handleSubmit(e)}>
        <select id="selectorID">
          <option id="plusID" defaultChecked>
            +
          </option>
          <option id="minusID">-</option>
        </select>
        <input
          type="text"
          id="descriptionID"
          ref={node => (textInput = node)}
          placeholder="Description"
        />
        <input type="number" id="valueID" ref={node => (numInput = node)} 
          placeholder="Value"/>
        <button type="submit">Add</button>
      </form>
    </>
  );
};
export default connect()(DescriptionBar);
