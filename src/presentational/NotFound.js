import React from 'react';
import { Link } from 'react-router-dom';

export default ({ staticContext = {} }) => {
  staticContext.status = 404;
  return (
  <div>
    <h1>Oops, nothing here!</h1>
    <Link to="/"><button type="submit">Home</button></Link>
  </div>
  );
};