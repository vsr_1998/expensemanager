import React from "react";

function Tables(props) {
  function showRow(description, value, index) {
    const deleteRow = (value, index) => {
      if (value < 0) props.deleteExpense(index);
      else props.deleteIncome(index);
    };
    return (
      <tr key={index} data-testid={index}>
        <td>
          <object style={{ float: "left", fontSize: 24 }}>{description}</object>
          <object style={{ float: "right", fontSize: 24 }}>{value}</object>
        </td>
        <button type="button" onClick={() => deleteRow(value, index)}>
          X
        </button>
      </tr>
    );
  }
  return (
    <div style={{ textAlign: "center", margin: "10px 0px 0px 0px" }}>
      <table data-testid="incomeTable" id="incomeListID" style={{ margin: "0px 30px 0px 0px" }}>
        <thead>
          <tr>
            <th
              style={{
                color: "purple",
                fontSize: 24
              }}
            >
              Income
            </th>
          </tr>
        </thead>
        <tbody>
          {props.incomeList.map(params =>
            showRow(params.text, params.value, params.index)
          )}
        </tbody>
      </table>

      <table data-testid="expenseTable" id="expenseListID" style={{ margin: "0px 0px 0px 30px" }}>
        <thead>
          <tr>
            <th
              style={{
                color: "red",
                fontSize: 24
              }}
            >
              Expense
            </th>
          </tr>
        </thead>
        <tbody>
          {props.expenseList.map(params =>
            showRow(params.text, params.value, params.index)
          )}
        </tbody>
      </table>
    </div>
  );
}

export default Tables;
